export interface go {
  "main": {
    "App": {
		ChangeMysql(arg1:string,arg2:string):Promise<boolean>
		CleanMysql():Promise<boolean>
		DeleteDatabase(arg1:string,arg2:string):Promise<boolean>
		ExitApp():Promise<void>
		GetDatabases(arg1:string):Promise<Array<string>>
		GetErorr():Promise<string>
		GetMySqlInfo():Promise<string>
		GetMysqlPath():Promise<string>
		HasMysql():Promise<boolean>
		InitMysql(arg1:string):Promise<boolean>
		NewDatabase(arg1:string,arg2:string):Promise<boolean>
		StartMysql():Promise<boolean>
		StopMysql():Promise<boolean>
		ToAddGroup():Promise<void>
    },
  }

}

declare global {
	interface Window {
		go: go;
	}
}
