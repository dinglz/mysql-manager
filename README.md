# MysqlManager

## 简介

go语言开发者喜爱选用web框架开发网站，并使用mysql作为数据库，然而面板体积过大，且大部分功能用不到。因而开发本工具

## 特点

轻盈，简洁，方便

注意：该程序仅用于Windows

## 编译

本项目采用[wails](https://wails.io/zh-Hans/docs/about)构建

调试运行

```sh
wails dev
```

编译

```sh
wails build
```

## 运行

1. 新建一个目录，名称中不包含中文

2. 将exe置于该目录

3. 将社区版的mysql文件夹放置在该目录，文件夹名称应当是mysql，不应该有版本号，mysql文件夹中应当包含bin文件夹。

   [社区版mysql下载](https://dev.mysql.com/downloads/mysql/)(注意下载zip archive而不是installer，否则您不能很方便的获取mysql目录并置于该目录)

4. 现在，您的目录中应当有***.exe(本程序)和mysql文件夹

5. 启动本程序。please enjoy！

## 展示

### 初始化

![](img/1.png)

![](img/2.png)

![](img/3.png)

![](img/4.png)

### 更改密码

![](img/5.png)

### 数据库管理

![](img/6.png)

![](img/7.png)

![](img/8.png)

![](img/9.png)

### 关于

![](img/10.png)

## 关于

开发者：丁丁(dinglz)

QQ：1149978856