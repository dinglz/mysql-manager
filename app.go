package main

import (
	"bytes"
	"context"
	"github.com/dingdinglz/dingtools/dingdb/dingmysql"
	"github.com/wailsapp/wails/v2/pkg/logger"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"syscall"
)

func PathExists(path string) bool {

	_, err := os.Stat(path)
	if err == nil {
		return true
	}
	if os.IsNotExist(err) {
		return false
	}
	return false
}

var errstr string

// App struct
type App struct {
	ctx context.Context
}

// NewApp creates a new App application struct
func NewApp() *App {
	return &App{}
}

// startup is called at application startup
func (b *App) startup(ctx context.Context) {
	// Perform your setup here
	b.ctx = ctx
}

// domReady is called after the front-end dom has been loaded
func (b *App) domReady(ctx context.Context) {
	// Add your action here
}

// shutdown is called at application termination
func (b *App) shutdown(ctx context.Context) {
	// Perform your teardown here
}

func (b *App) GetMySqlInfo() string {
	mysqlpath, _ := filepath.Abs(filepath.Join("mysql", "bin"))
	command := exec.Command("cmd", "/c", "mysql.exe", "--version")
	command.Dir = mysqlpath
	command.SysProcAttr = &syscall.SysProcAttr{HideWindow: true}
	var out bytes.Buffer
	command.Stdout = &out
	err := command.Run()
	if err != nil {
		return err.Error()
	}
	//exec.Command()
	return out.String()
}

func (b *App) GetErorr() string {
	return errstr
}
func (b *App) InitMysql(pass string) bool {
	path, _ := filepath.Abs(filepath.Join("mysql"))
	dingmysql.SetMysqlPath(path)
	err, _ := dingmysql.Mysql_Init()
	if err != nil {
		errstr = err.Error()
		return false
	}
	err, _ = dingmysql.Mysql_start()
	if err != nil {
		errstr = err.Error()
		return false
	}
	err, _ = dingmysql.Mysql_updatePass("", pass)
	if err != nil {
		errstr = err.Error()
		return false
	}
	return true
}

func (b *App) StartMysql() bool {
	path, _ := filepath.Abs(filepath.Join("mysql"))
	dingmysql.SetMysqlPath(path)
	err, _ := dingmysql.Mysql_start()
	if err != nil {
		errstr = err.Error()
		return false
	}
	return true
}

func (b *App) StopMysql() bool {
	path, _ := filepath.Abs(filepath.Join("mysql"))
	dingmysql.SetMysqlPath(path)
	err, _ := dingmysql.Mysql_stop()
	if err != nil {
		errstr = err.Error()
		return false
	}
	return true
}

func (b *App) CleanMysql() bool {
	path, _ := filepath.Abs(filepath.Join("mysql"))
	dingmysql.SetMysqlPath(path)
	err, _ := dingmysql.Mysql_remove()
	if err != nil {
		errstr = err.Error()
		return false
	}
	err = dingmysql.Mysql_Clean()
	if err != nil {
		errstr = err.Error()
		return false
	}
	return true
}

func (b *App) ChangeMysql(old string, new string) bool {
	path, _ := filepath.Abs(filepath.Join("mysql"))
	dingmysql.SetMysqlPath(path)
	err, _ := dingmysql.Mysql_updatePass(old, new)
	if err != nil {
		errstr = err.Error()
		return false
	}
	return true
}

func (b *App) GetDatabases(pass string) []string {
	return dingmysql.Mysql_GetDatabases(pass)
}

func (b *App) HasMysql() bool {
	path, _ := filepath.Abs(filepath.Join("mysql"))
	return PathExists(path)
}

func (b *App) GetMysqlPath() string {
	if !b.HasMysql() {
		return ""
	}
	path, _ := filepath.Abs(filepath.Join("mysql"))
	return path
}

func (b *App) DeleteDatabase(pass string, name string) bool {
	defaultLogger := logger.NewDefaultLogger()
	defaultLogger.Debug(pass + "     " + name)
	path, _ := filepath.Abs(filepath.Join("mysql"))
	dingmysql.SetMysqlPath(path)
	err, _ := dingmysql.Mysql_DropDatabase(name, pass)
	if err != nil {
		errstr = err.Error()
		return false
	}
	return true
}

func (b *App) NewDatabase(name string, pass string) bool {
	path, _ := filepath.Abs(filepath.Join("mysql"))
	dingmysql.SetMysqlPath(path)
	err, _ := dingmysql.MySql_CreateDatabase(strings.ReplaceAll(name, "\n", ""), pass)
	if err != nil {
		errstr = err.Error()
		return false
	}
	return true
}

func (b *App) ToAddGroup() {
	command := exec.Command("cmd", "/c", "start", `https://jq.qq.com/?_wv=1027^&k=hPDKaH3L`)
	command.SysProcAttr = &syscall.SysProcAttr{HideWindow: true}
	command.Run()
}

func (b *App) ExitApp() {
	os.Exit(1)
}
